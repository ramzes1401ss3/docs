# Requirements before installation

We recommend installing Yucca on the Ubuntu operating system with long-term support (LTS), for example, 18.04 and older, and it is possible to work on any other Linux distribution. You will also need Docker to work. the instructions for installing it are available on the [official site] (https://docs.docker.com/engine/install/).

## Installation using a script

Download and run the script:
```
wget https://get.yucca.app/latest/yucca-install.sh
bash ./yucca-install.sh
```
If `curl` is present, the same thing is done with a single command:
```
bash <(curl -fsSL https://get.yucca.app/latest/yucca-install.sh)
```
Wait for the script to finish executing.

You can also use it to delete Yucca:
```
bash ./yucca-install.sh remove
```

## Manual installation

Let's start by creating a user and group to avoid running Yucca from the root privileged user, and adding a new user to the docker group to be able to work with Docker containers:

```
sudo groupadd -f yucca
sudo useradd -M -s /bin/false -b /opt/yucca -g yucca -G docker yucca
```

Next, create a folder for installation, set the owner and access rights:

```
sudo mkdir -p /opt/yucca
sudo chown yucca:yucca /opt/yucca
sudo chmod 2775 /opt/yucca
```

Go to the created folder, download the latest version of Yucca, config file and the systemd unit file of the service:

```
sudo cd /opt/yucca
wget https://get.yucca.app/latest/yucca -O /opt/yucca/yucca
sudo chmod +x /opt/yucca/yucca
wget https://get.yucca.app/latest/yucca.toml -O /opt/yucca/yucca.toml
wget https://get.yucca.app/latest/yucca.service -O /opt/yucca/yucca.service
```

Copy the systemd unit file to the service directory and update the list of daemons so that the changes take effect:

```
sudo cp /opt/yucca/yucca.service /etc/systemd/system/
sudo systemctl daemon-reload
```

Now you can launch Yucca and check how it works:

```
sudo systemctl enable yucca
sudo systemctl start yucca
```

For CentOS/RHEL/Fedora, you must configure the network filter:

```
sudo firewall-cmd --permanent --new-service=yucca
sudo firewall-cmd --permanent --service=yucca --add-port=9910/tcp
sudo firewall-cmd --permanent --zone=public --add-service=yucca
sudo firewall-cmd --reload
```

After launching, the WEB interface will be available at <http://ip-вашего-сервера:9910>.

