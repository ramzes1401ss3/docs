# 0.14.0 + https://github.com/dauxio/daux.io/commit/c3523c85394333ad832a7aa1197ab9eeb4492cec
FROM daux/daux.io@sha256:9d1dc4bf6e3d62092da6f2d1e6b869b932e5095fd99c97f44ab51c77a1f2514f as build
WORKDIR /build

COPY . ./

RUN set -xe \
    && mkdir -p ./static \
    && daux generate -c config.json --source=./  --destination=./static --no-interaction

FROM nginx:alpine as release
COPY --from=build /build/static/ /var/www/html
COPY ./nginx_default.conf /etc/nginx/conf.d/default.conf
LABEL io.yucca.docs=true
EXPOSE 8086
