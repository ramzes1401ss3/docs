#!/usr/bin/env bash

set -xe

SCRIPT_DIR="$(dirname $(readlink -f $0))"

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-'registry.gitlab.com/yuccastream/sites/docs'}
CI_COMMIT_SHA=${CI_COMMIT_SHA:-$(git rev-parse HEAD)}

helpInfo() {
  echo -e "Первый аргумент указывает, деплоить или удалять \n"
  echo -e "apply | remove"
}

deploy() {
    cd ${SCRIPT_DIR}/k8s
    git checkout @ -- kustomization.yaml
    kustomize edit set image registry.gitlab.com/yuccastream/sites/docs=${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHA}
    kustomize build
    kustomize build | kubectl ${1} -f-
    git checkout @ -- kustomization.yaml
    echo -e '\e[0;33m' "URL:" '\e[0;32m' "https://docs.yucca.app" '\e[0m'
}

case "${1}" in
    apply ) deploy apply ;;
    remove ) deploy delete ;;
    * ) helpInfo ;;
esac
