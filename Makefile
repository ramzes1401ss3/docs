ifeq ($(CI_REGISTRY_IMAGE),)
CI_REGISTRY_IMAGE = registry.gitlab.com/yuccastream/sites/docs
endif

ifeq ($(VERSION),)
VERSION = latest
endif

ifeq ($(CI_COMMIT_SHA),)
CI_COMMIT_SHA = $(shell git rev-parse HEAD)
endif

.DEFAULT_GOAL := local

.PHONY: local
local: build
local:
	@echo '\e[0;32m'"==> local build and check "'\e[0m'
	@echo '\e[0;36m'"==> check on http://localhost:8086"'\e[0m'
	docker run --rm  -p 8086:8086 $(CI_REGISTRY_IMAGE):$(VERSION)

.PHONY: build
build:
	@echo '\e[0;32m'"==> Build docker image"'\e[0m'
	docker build --force-rm \
		-t $(CI_REGISTRY_IMAGE):$(VERSION) \
		-t $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA) \
		-f Dockerfile .

.PHONY: deploy
deploy: build
deploy:
	docker push $(CI_REGISTRY_IMAGE):$(VERSION)
	docker push $(CI_REGISTRY_IMAGE):$(CI_COMMIT_SHA)
	bash ./.deploy/deploy.sh apply

.PHONY: only-deploy
only-deploy:
	bash ./.deploy/deploy.sh apply

.PHONY: remove
remove:
	bash ./.deploy/deploy.sh remove

.PHONY: serve
serve:
	# 0.14.0 + https://github.com/dauxio/daux.io/commit/c3523c85394333ad832a7aa1197ab9eeb4492cec
	docker run --rm -it -v $(PWD):/docs -w /docs -p 8085:8085 daux/daux.io@sha256:9d1dc4bf6e3d62092da6f2d1e6b869b932e5095fd99c97f44ab51c77a1f2514f /bin/bash -c 'daux serve -c config.json --source ./ --host 0.0.0.0'
