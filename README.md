# Docs

User manual site

<https://docs.yucca.app>
<https://docs.yuccastream.com>

Для просмотра изменений, запусить:  
`make `
Локально доступно на <http://localhost:8086>  

Сборка и docker push:  
`make build`

Сборка и deploy в k8s:  
`make deploy`

 Deploy (kubectl apply):  
`make only-deploy`


 Remove (kubectl remove):  
`make remove`
