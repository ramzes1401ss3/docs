[TOC]

Настройка Yucca может осуществляться через конфигурационный файл, аргументы командной строки или переменные окружения.

Приоритет при чтении конфигурации:

1. Аргументы командной строки
2. Файл
3. Переменные окружения

# Таблица значений

Параметры делятся на логические разделы (блоки), которые соответствуют префиксу в аргументах командной строки и переменных окружения.

В разделе *Файл конфигурации* в квадратных скобках `[]` указывается блок конфигурации к которому пренадлежит опция.

## server

Глобальные параметры сервера.  

| <div style="width:230px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--log-json` | - | - | true | Выводить логи в JSON формате |
| `--log-level` | - | - | "info" | Уровень логирования. Возможные значения (panic, fatal, error, warning, info, debug)  |
| `--help` | - | - | - | Вызов справки Yucca Server (Пример: ./yucca server --help) |
| `--config` | - | - | "/etc/yucca/yucca.toml" | Путь к файлу конфигурации |
| `--data-dir` | [server] data_dir | YUCCA_SERVER_DATA_DIR | "" | Глобальный каталог для хранения состояния (в случае использования sqlite3) и данных стримов. По умолчанию создается под названием `data` в том же пути, что и бинарный файл `yucca`. |
| `--alloc-dir` | [server] alloc_dir | YUCCA_SERVER_ALLOC_DIR | "" | Каталог с данными стримов (видео архив и прочее). По умолчанию создается под названием `alloc` в data_dir. |
| `--state-dir` | [server] state_dir | YUCCA_SERVER_STATE_DIR | "" | Каталог с файлом базы данных (в случае использования sqlite3). По умолчанию создается под названием `server` в data_dir. |
| `--default-language` | [server] default_language |YUCCA_SERVER_DEFAULT_LANGUAGE | "en" | Язык интерфейса по умолчанию у новых пользователей |
| `--web.listen-address` | [server] listen_address | YUCCA_SERVER_LISTEN_ADDRESS | "127.0.0.1:9910" | IP адрес и порт на котором будет доступен веб интерфейс |
| `--cert-file` | [server] cert_file | YUCCA_SERVER_CERT_FILE | "" | Путь к файлу сертификата, в случае использования HTTPS |
| `--cert-key` | [server] cert_key | YUCCA_SERVER_CERT_KEY | "" | Путь к файлу ключа, в случае использования HTTPS |
| `--pprof` |[server] pprof | YUCCA_SERVER_PPROF | true | Отладочный режим |

## database

Подключение к базе данных.  

Поддерживается 2 типа баз данных для хранения состояния:

 - sqlite3
 - postgres

В небольших инсталляциях, достаточно `sqlite3`, при большом количестве стримов, мы рекомендуем использовать `postgres`.

| <div style="width:230px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--database-type` | [database] type | YUCCA_DATABASE_TYPE | "sqlite3" | Тип используемой базы данных (sqlite3 или postgres) |
| `--database-path` | [database] path | YUCCA_DATABASE_PATH | "" | Путь к файлу базы (только для sqlite3) |
| `--database-host` | [database] host | YUCCA_DATABASE_HOST | "127.0.0.1:5432" | Адрес подключения к базе (только для postgres) |
| `--database-name` | [database] name | YUCCA_DATABASE_NAME | "yucca" | Название базы данных (только для postgres) |
| `--database-user` | [database] user | YUCCA_DATABASE_USER | "postgres" | Имя пользователя для подключения (только для postgres) |
| `--database-password` | [database] password | YUCCA_DATABASE_PASSWORD | "postgres" | Пароль пользователя для подключения (только для postgres) |
| `--database-ssl-mode` | [database] ssl_mode | YUCCA_DATABASE_SSL_MODE | "disable" | Использовать при подключении SSL (шифрованное соединение) (только для postgres). Возможные значения (disable, require, verify-full) |
| `--database-busy-timeout` | [database] busy_timeout | YUCCA_DATABASE_BUSY_TIMEOUT | 500 | Тайм-аут в секундах для ожидания разблокировки таблицы SQLite (только для sqlite3) |
| `--database-ca-cert-path` | [database] ca_cert_path | YUCCA_DATABASE_CA_CERT_PATH | "/etc/ssl/certs" | Путь к сертификату центра сертификации (CA) случае использования SSL (только для postgres) |
| `--database-client-cert-path` | [database] client_cert_path | YUCCA_DATABASE_CLIENT_CERT_PATH | "" | Путь к файлу с сертификатом в случае использования SSL (только для postgres) |
| `--database-client-key-path` | [database] client_key_path | YUCCA_DATABASE_CLIENT_KEY_PATH | "" | Путь к файлу с ключом в случае использования SSL (только для postgres) |
| `--database-cache-mode` | [database] cache_mode | YUCCA_DATABASE_CACHE_MODE | "shared" | Параметр общего кэша, используемый для подключения к базе данных (только для sqlite3). Возможные значения (private, shared) |
| `--database-conn-max-lifetime `| [database] conn_max_lifetime | YUCCA_DATABASE_CONN_MAX_LIFETIME | "0s" | Устанавливает максимальное время, в течение которого соединение может быть повторно использовано. |
| `--database-log-queries` | [database] log_queries | YUCCA_DATABASE_LOG_QUERIES | false | Логировать sql вызовы и время выполнения |
| `--database-max-open-conn` | [database] max_open_conn | YUCCA_DATABASE_MAX_OPEN_CONN | 0 | Максимальное количество открытых соединений с базой данных (только для postgres) |
| `--database-max-idle-conn` | [database] max_idle_conn | YUCCA_DATABASE_MAX_IDLE_CONN | 2 | Максимальное количество соединений в ожидании (только для postgres) |

## executor

Параметры запуска стримов.

| <div style="width:230px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--executor` | [executor] type | YUCCA_EXECUTOR_TYPE | "docker" | Тип исполнителя, для парковки стримов. Возможные значения (docker) |
| `--docker-image` | [executor.docker] image | YUCCA_EXECUTOR_DOCKER_IMAGE | "registry.gitlab.com/yuccastream/yucca" | Имя докер образа для работы в качестве исполнителя |
| `--docker-cap-add` | [executor.docker] cap_add | YUCCA_EXECUTOR_DOCKER_CAP_ADD | "[]" | Добавить Linux capabilities для докер контейнера |
| `--docker-cap-drop` | [executor.docker] cap_drop | YUCCA_EXECUTOR_DOCKER_CAP_DROP | "[]" | Сбросить Linux capabilities для докер контейнера |
| `--docker-cpu-shares` | [executor.docker] cpu_shares | YUCCA_EXECUTOR_DOCKER_CPU_SHARES | 0 | Ограничение потребления процессора для контейнера. Доли процессора (относительный вес по сравнению с другими контейнерами) |
| `--docker-dns` | [executor.docker] dns | YUCCA_EXECUTOR_DOCKER_DNS | "[]" | Список DNS-серверов для использования контейнером |
| `--docker-dns-search` | [executor.docker] dns_search | YUCCA_EXECUTOR_DOCKER_DNS_SEARCH | "[]" | Список DNS-доменов поиска |
| `--docker-memory` | [executor.docker] memory | YUCCA_EXECUTOR_DOCKER_MEMORY | 0 | Ограничение памяти для контейнера (Memory limit) |
| `--docker-memory-reservation` | [executor.docker] memory_reservation | YUCCA_EXECUTOR_DOCKER_MEMORY_RESERVATION | 0 | Мягкий предел памяти (Memory soft limit) |
| `--docker-memory-swap` | [executor.docker] memory_swap | YUCCA_EXECUTOR_DOCKER_MEMORY_SWAP | 0 | Общий предел памяти (Total memory limit (memory + swap)) |
| `--docker-nano-cpus` | [executor.docker] nano_cpus | YUCCA_EXECUTOR_DOCKER_NANO_CPUS | 0 | Квота процессора в единицах |
| `--docker-network-driver` | [executor.docker] network_driver | YUCCA_EXECUTOR_DOCKER_NETWORK_DRIVER | "bridge" | Сетевой драйвер Docker (https://docs.docker.com/network/) |
| `--docker-oom-kill-disable` | [executor.docker] oom_kill_disable | YUCCA_EXECUTOR_DOCKER_OOM_KILL_DISABLE | false | Не уничтожать процессы в контейнере, если возникает ошибка нехватки памяти (OOM) |
| `--docker-oom-score-adjust` | [executor.docker] oom_score_adjust | YUCCA_EXECUTOR_DOCKER_OOM_SCORE_ADJUST | 0 | Настройка показателей OOM |
| `--docker-security-opt` | [executor.docker] security_opt | YUCCA_EXECUTOR_DOCKER_SECURITY_OPT | "[]" | Список строковых значений для настройки меток системы MLS, таких как SELinux |

## streams

Параметры относящиеся к поведению стримов.

| <div style="width:300px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--streams-archive-download-max-duration` | [streams] archive_download_max_duration | YUCCA_STREAMS_ARCHIVE_DOWNLOAD_MAX_DURATION | "24h" | Максимальная допустимая длина запрашиваемого отрезка времени для скачивания |
| `--streams-playlist-max-duration` | [streams] playlist_max_duration | YUCCA_STREAMS_PLAYLIST_MAX_DURATION | "3h" | Максимально допустимая длина запрашиваемого отрезка времени для m3u8 плейлиста |
| `--streams-ranges-max-duration` | [streams] ranges_max_duration | YUCCA_STREAMS_RANGES_MAX_DURATION | "168h" | Максимальная допустимая длина запрашиваемого отрезка времени для рейнджей (диапазон доступности архива) |
| `--streams-uuid-slug` | [streams] uuid_slug | YUCCA_STREAMS_UUID_SLUG | false | Использовать, в качестве публичного идентификатора потока, сгенерированный UUID |

## telemetry

Телеметрия.

| <div style="width:300px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--telemetry` | [telemetry] enabled | YUCCA_TELEMETRY_ENABLED | true | Включить / отключить телеметрию |
| `--web.telemetry-path` | [telemetry] path | YUCCA_TELEMETRY_PATH | "/metrics" | URI для доступа к телеметрии |
| `--telemetry-basic-auth-username` | [telemetry] basic_auth_username | YUCCA_TELEMETRY_BASIC_AUTH_USERNAME | "" | Логин базовой авторизации для доступа к телеметрии |
| `--telemetry-basic-auth-password` | [telemetry] basic_auth_password | YUCCA_TELEMETRY_BASIC_AUTH_PASSWORD | "" | Пароль базовой авторизации для доступа к телеметрии |

## branding

Брендирование.

*Доступно только в Enterprise*

| <div style="width:230px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--branding-app-logo` | [branding] app_logo | YUCCA_BRANDING_APP_LOGO | "" | Путь к файлу с логотипом |
| `--branding-app-title` | [branding] app_title | YUCCA_BRANDING_APP_TITLE | "" | Текст в поле Title |
| `--branding-fav-icon` | [branding] fav_icon | YUCCA_BRANDING_FAV_ICON | "" | Путь к файлу с favicon |

## analytics

Сбор аналитики.

Yucca сервер может собирать и отсылать маркетинговую информацию. В частности информацию о конфигурации системы (модель процессора, тип и версия операционной системы, размер оперативной памяти, размер дисков, количество запущенных потоков) на которой он запущен. Мы НЕ собираем информацию об источниках стримов и прочие чувствительные данные. Сбор аналитики помогает нам лучше понять наших пользователей.

Информация о сборе также есть в [условиях использования](https://yucca.app/terms).

| <div style="width:230px">Аргументы командной строки</div> | Файл конфигурации | Переменные окружения | Значение по умолчанию | <div style="width:300px">Пояснение</div> |
| :------------------------- | :---------------- | :------------------- | :-------------------- | :-------- |
| `--analytics` | [analytics] enabled | YUCCA_ANALYTICS_ENABLED | true | Включить / отключить|
| `--analytics-address` | [analytics]  | YUCCA_ANALYTICS_ADDRESS | "https://analytics.yuccastream.com/" | URL на который отправлять информацию |
